import allure
import random
import pytest
from enum import Enum


class Mode(Enum):
    ADD = '+'
    SUB = '-'
    MUL = '*'
    DIV = '/'


class Calculator:
    def __init__(self, mode: Mode|None):
        if mode is None:
            self._mode = Mode.ADD
        else:
            self._mode = mode

    def change_mode(self, mode: Mode):
        self._mode = mode

    def operate(self, a, b):
        match self._mode:
            case Mode.ADD:
                return a + b
            case Mode.SUB:
                return a - b
            case Mode.MUL:
                return a * b
            case Mode.DIV:
                return a / b
            case _:
                return NotImplemented


@pytest.fixture(scope="session")
@allure.title("Prepare calculator")
def calc_sub():
    return Calculator(Mode.SUB)


def test_subtraction(calc_sub):
    with allure.step("Number 1 gen"):
        a = random.randint(1, 10)
    with allure.step("Number 2 gen"):
        b = random.randint(1, 10)
    with allure.step("Calculation!!!!"):
        assert calc_sub.operate(a, b) == a - b
